# product



## Getting started


## Install the required dependencies for the frontend with reactjs :


```
 cd product_frontend
npm install
 npm start
```

## Install the required dependencies for the backend with Laravel:

```
 cd product_backend
 composer install 
 php artisan db:seed --class=ProductSeeder
 php artisan serve
```


